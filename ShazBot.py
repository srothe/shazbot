import socket
import urllib2
import urllib
import json
import datetime

class ShazBot(object):
    def __init__(self, nick, chan=None):
        self.nick = nick
        self.admins = [":Shazer[2]", ":MatthewJA"]
        self.cmd = {"quit":self.quitchan}
        self.join()
        if chan:
            self.join_chan(chan)

    def join(self):
        self.con = socket.create_connection(("irc.freenode.net", 6667))
        self.con.send("USER %s %s %s :%s\n" % (self.nick, self.nick, self.nick, self.nick))
        self.con.send("NICK %s\n" % (self.nick))

    class ShazbotQuit(Exception):
        pass

    def join_chan(self, chan):
        self.chan = chan
        self.con.send("JOIN %s\n" %(chan))

    def run(self):
        while True:
            try:
                ircmsg = self.con.recv(2048)
                ircmsg = ircmsg.strip("\r\n")
                split = ircmsg.split(" ")
                print ircmsg
                if split[1] == "PRIVMSG":
                    self.readmsg(split[0], split[2], split[3:])
            except self.ShazbotQuit:
                print "Quitting..."
                break
            except Exception, e:
                print "Exception!", e

    def readmsg(self, who, chan, msg):
        who = who.split("!")
        if chan == self.nick:
            self.sendmsg(who[0].split(":")[1], "Yes.")
        else:   
            if msg[0] == ":!sb":
                if who[0] in self.admins:
                    if msg[1] in self.cmd:
                        self.cmd[msg[1]]()
                    else:
                        self.sendmsg(chan, "That function doesn't exist!")
                else:
                    self.sendmsg(chan, "You can not take control of me.")

    def sendmsg(self, chan, msg):
        self.con.send("PRIVMSG %s :%s\n"%(chan, msg))

    def quitchan(self):
        self.con.send("QUIT GOODBYE\n")
        raise self.ShazbotQuit
        
SB = ShazBot("ShazBot1", "##NCSS_Challenge")
SB.join_chan("#python")
SB.run()
